History is a replacement for the revision list that is built into the node
module.

With the history module you can configure which columns are displayed in the
revision list and how they are formated. A hook is provided for other modules
to add columns or format rows and columns.

Installation
------------

1. Copy this whole folder to modules directory, as usual. Drupal should
   automatically detect the module. Enable the module on the modules'
   administration page.

2. Go to admin/settings/history to configure which columns you want to be
   visible and how they are formated. The default configuration resembles the
   original revision list.

Advanced Usage
---------

Implement hook_history to add or format columns to your liking.


Authors
-------
Originally written by Oswald Jaskolla <oswald.jaskolla [at] schieferdecker.com>.
