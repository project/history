<?php

/**
 * This is an example of hook_history().
 * 
 * @author Oswald Jaskolla <oswald.jaskolla@schieferdecker.com>
 * @date 2006-10-24
 * @file
 */

/**
 * Allows modules to modify the history list.
 * 
 * Operations can be
 * - 'info' To request information about the columns provided by this module
 * - 'column' To request a specific column
 * - 'class' To reqest a list of css classes to format the rows.
 * Details follow.
 *
 * For the operation 'info':
 * - parameter $nid is null,
 * - parameter $args is null,
 * - an array of column information must be returned. Keys of this array are 
 *   column identifiers. Different modules can use the same column identifiers
 *   without collision. A column information is an arrays with the following
 *   keys:
 *   + 'title'
 *     A string with the title of the column
 *   + 'weight'
 *     The default weight of the column
 *   + 'description'
 *     A description of the column
 *   + 'class'
 *     css class of each item in the column (including the title item)
 *
 * For the operation 'column':
 * - $nid is the nid of the node whose history is displayed
 * - $args is an array()
 *   $args[0] is the column identifier
 *   $args[1] is an array of revision identifiers (vid) that are displayed
 * - an array of HTML must be returned. Keys of this array are the vids of the
 *   revisions, values are the cell contents.
 *
 * For the operation 'class':
 * - parameters are the same as with operation 'column';
 * - an array of strings must be returned. Keys of this array are the vids of
 *   the revisions, values are css class names for the rows.
 *
 * @param string $op The operation to perform
 * @param int $nid The nid of the node whose revision list is displayed
 * @param mixed $args Additional arguments
 * @return array See details
 */
function hook_history($op, $nid = null, $args = null) {
  switch ($op) {
    case 'info':
      $columns = array();
      $columns['ventilate'] = array(
        'title' => t('ventilate revision'),
        'description' => t('Allows to ventilate specific revisions of a node'),
        'weight' => 5,
        'class' => 'clButton',
      );
      $colums['in charge'] = array(
        'title' => t('in charge'),
        'description' => t('Links to the user in charge of the revision'),
        'weight' => 3,
        'class' => 'clUserLink',
      );
      return $colums;
    case 'column':
      $cid = $args[0];
      $vids = $args[1];
      $cells = array();
      switch ($cid) {
        case 'ventilate':
          foreach ($vids as $vid) {
            if (fan_node_is_ventilated($nid, $vid)) {
              $cells[$vid] = t('is ventilated');
            } else {
              $cells[$vid] = fan_ventilate_link($nid, $vid);
            }
          }
          break;
        case 'in charge':
          foreach ($vids as $vid) {
            if (fan_node_is_ventilated($nid, $vid)) {
              $uid = fan_node_get_ventilator($nid, $vid);
              $cells[$vid] = fan_vantilator_link($uid);
            } else {
              $cells[$vid] = t('none');
            }
          }
          break;
      }
      return $cells;
    case 'class':
      $cid = $args[0];
      $vids = $args[1];
      $classes = array();
      switch ($cid) {
        case 'ventilate':
          foreach ($vids as $vid) {
            if (fan_node_is_ventilated($nid, $vid)) {
              $cells[$vid] = 'clFresh';
            }
          }
          break;
        case 'in charge':
          break;
      }
      return $classes;
    default:
  }
}
